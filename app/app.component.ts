
class AppController implements ng.IController{
    users: any;
    static $inject = ["$state"];
    constructor(public $state:ng.ui.IStateService){
        $state.go("app.account");
    }
}

export class AppComponent implements ng.IComponentOptions{
    controller:any;
    static NAME:string = 'appView';
    templateUrl:any;
    
    constructor(){
       this.controller = AppController;
       this.templateUrl = require("./app.component.html");  
    }
    
}
