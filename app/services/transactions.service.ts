  interface ITransactionsService {
    getTransactions(accountId: string, from: string, to: string): any;
  }

  export class TransactionsService implements ITransactionsService {
    static $inject = ['$q', "$http"];
    static NAME = "transactionsService"
    constructor(protected $q: ng.IQService, protected $http: ng.IHttpService) { 
      $http.defaults.headers.common.Authorization = "Bearer ASHWLIkouP2O6_bgA2wWReRhletgWKHYjLqDaqb0LFfamim9RjexTo22ujRIP_cjLiRiSyQXyt2kM1eXU2XLFZQ0Hro15HikJQT_eNeT_9XQ";
    }
    
    public getTransactions(accountId: string, from: string, to: string){
            return this.$http.get("https://api.figo.me/rest/accounts/" + accountId + "/transactions" + "?since=" + from + "&until=" + to)
                    .then((response: any): any => {
                      return response.data;
                    });
        }
  }
