interface IAccountService {
  getAccounts(): any;
}

export class AccountService implements IAccountService {
  static $inject = ['$q', "$http"];
  static NAME = "accountService"
  constructor(protected $q: ng.IQService, protected $http: ng.IHttpService) {
    $http.defaults.headers.common.Authorization = "Bearer ASHWLIkouP2O6_bgA2wWReRhletgWKHYjLqDaqb0LFfamim9RjexTo22ujRIP_cjLiRiSyQXyt2kM1eXU2XLFZQ0Hro15HikJQT_eNeT_9XQ";
  }

  public getAccount(accountId: string) {
    return this.$http.get("https://api.figo.me/rest/accounts/" + accountId)
      .then((response: any): any => {
        return response.data;
      });
  }

  public getAccounts() {
    return this.$http.get("https://api.figo.me/rest/accounts")
      .then((response: any): any => {
        return response.data;
      });
  }
}
