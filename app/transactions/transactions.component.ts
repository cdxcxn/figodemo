import { TransactionsService } from "../services/transactions.service";
import { AccountService } from "../services/account.service";

class TransactionsController implements ng.IController {
    public transactions: any;
    public dtFrom: Date;
    public dtTo: Date;
    public dpFrom: any = { opened: false };
    public dpTo: any = { opened: false };;
    public format: string = "yyyy-MM-dd";;
    public dateOptions: any;
    public loading: boolean;
    public gridOptions: uiGrid.IGridOptions = {
        enableSorting: true,
        paginationPageSizes: [50],
        paginationPageSize: 50,
        columnDefs: [
            { field: 'type', enableSorting: false, enableHiding: false },
            { field: 'value_date', enableSorting: false, enableHiding: false },
            { field: 'currency', enableSorting: false, enableHiding: false },
            { field: 'amount', enableHiding: false }
        ]
    };
    public paramId: string;
    public paramFrom: string;
    public paramTo: string;
    public account: any;
    public typesChart: any = [];
    public cuantities: any = [];
    public totalAmount: number = 0;

    static $inject = ["transactionsService", "accountService", "$state"];
    constructor(public transactionsService: TransactionsService,
                public accountService: AccountService,
                public $state: ng.ui.IStateService) {
        this.paramId = $state.params.id;
        this.paramFrom = $state.params.from;
        this.paramTo = $state.params.to;
        this.doAccountRequest(this.paramId);
        this.doTransactionsRequest(this.dtFrom, this.dtTo);        
    }

    public openFrom(): void {
        this.dpFrom.opened = true;
    }

    public openTo(): void {
        this.dpTo.opened = true;
    }

    public changeDate() {
        this.doTransactionsRequest(this.dtFrom, this.dtTo);
    }

    public doTransactionsRequest(from: Date, to: Date): void {
        /// Set time to the end of the day
        to ? to.setHours(11, 59, 59, 999) : "";
        this.loading = true;
        let miDate: Date;
        let maDate: Date;
        this.transactionsService.getTransactions(this.paramId, from ? from.toISOString() : "", to ? to.toISOString() : "")
            .then((data) => {
                this.transactions = [];
                let transDate: Date;
                for (let tra of data.transactions) {
                    var tran = {};

                    tran["type"] = tra["type"];
                    tran["amount"] = tra["amount"];
                    this.cuantities[tra["type"]] = tra["amount"] + (this.cuantities[tra["type"]] ? this.cuantities[tra["type"]] : 0);
                    this.totalAmount += tra["amount"];

                    tran["value_date"] = tra["value_date"];
                    transDate = new Date(tra["value_date"]);
                    if (!miDate || miDate > transDate)
                        miDate = transDate;
                    if (!maDate || maDate < transDate)
                        maDate = transDate;
                    tran["currency"] = tra["currency"];                    
                    this.transactions.push(tran);
                }

                this.dtFrom = from ? from : miDate;
                this.dtTo = to ? to : maDate;
                this.dateOptions = {
                    startingDay: 1
                };                
            })
            .finally(() => {
                this.gridOptions.data = this.transactions;
                this.loading = false;
                this.buildTypes();
            });
    }

    public doAccountRequest(accountId: string): void {
        this.accountService.getAccount(accountId)
            .then((data) => {
                this.account = data;
            });
    }

    public buildTypes(): void {      
        for (let key in this.cuantities) {
            this.typesChart.push({ type: key, value: this.cuantities[key] });
        }
    }
}

export class TransactionsComponent implements ng.IComponentOptions {
    controller: any;
    static NAME: string = 'transactionsView';
    templateUrl: any;

    constructor() {
        this.controller = TransactionsController;
        this.templateUrl = require("./transactions.html");
    }
}
