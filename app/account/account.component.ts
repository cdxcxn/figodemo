import { AccountService } from "../services/account.service";

class AccountController implements ng.IController {
    accounts: any[];    
    static $inject = ["accountService", "$scope"];
    constructor(public accountService: AccountService, public $scope: ng.IScope) {
        $scope.loading = true;
        $scope.gridOptions = {};
        this.accountService.getAccounts()
            .then((data) => {
                this.accounts = data.accounts;
            })
            .finally(() => {
                $scope.loading = false;
            });
    }
}

export class AccountComponent implements ng.IComponentOptions {
    controller: any;
    static NAME: string = 'accountView';
    templateUrl: any;

    constructor() {
        this.controller = AccountController;
        this.templateUrl = require("./account.html");
    }

}

