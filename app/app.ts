// app.ts
import { module, element, bootstrap, ILogService } from 'angular';
import { AppComponent } from "../app/app.component"
import { TransactionsComponent } from "./transactions/transactions.component";
import { AccountComponent } from "./account/account.component";
import { AccountService } from "./services/account.service";
import { TransactionsService } from "./services/transactions.service";

import './app.less';

export let app = module('app', [
    "ui.router",
    "ui.grid",
    'ui.grid.pagination',
    'ui.bootstrap'
])   
    .config(["$stateProvider", "$urlRouterProvider", ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) => {
        $stateProvider
            .state({
                name: 'app',
                url: '/app',
                component: AppComponent.NAME
            })
            .state({
                name: 'app.transactions',
                params: {
                    id: null
                },
                url: '/transactions',                
                component: TransactionsComponent.NAME
            })
            .state({
                name: 'app.account',
                url: '/account',
                component: AccountComponent.NAME
            });
        $urlRouterProvider.otherwise('/app');
    }])
    .component(AppComponent.NAME, new AppComponent())
    .component(TransactionsComponent.NAME, new TransactionsComponent())
    .component(AccountComponent.NAME, new AccountComponent())
    .service(AccountService.NAME, AccountService)
    .service(TransactionsService.NAME, TransactionsService);  

element(document).ready(function () {
    bootstrap(document, ["app"]);
});